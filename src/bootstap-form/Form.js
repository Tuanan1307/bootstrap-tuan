import React from "react";
import { Container, Row, Col, Form, Card } from "react-bootstrap";

import { BsFillCaretDownFill } from "react-icons/bs";
const Forms = () => {
  return (
    <div className="form">
      <div class="header-form w-100 position-fixed top-0">
        <div
          class="header-background w-100 position-fixed"
          style={{
            background: "#153d77",
            height: "200px",
            zIndex: 1000,
          }}
        ></div>
      </div>

      <div
        className="form-header border rounded-3 m-3 py-3 position-relative top-0 start-0"
        style={{
          background: "#fff",
        }}
      >
        <Container fluid>
          <div className="title h4 ms-1 ">XXXX</div>
          <Row
            className="border m-1 rounded-3 mt-4"
            style={{ background: "#e9ecef" }}
          >
            <Col md={4}>
              <Form as={Col} md={10}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-2">XXXX</Form.Label>
                  <Form.Control type="date" />
                  &nbsp;<span className="mt-2">~</span>&nbsp;
                  <Form.Control type="date" />
                </Form.Group>
              </Form>
              <Form as={Col} md={10}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">XX</Form.Label>
                  {/* <Form.Control type="text" className="w-25" /> */}
                  <input type="text" className="w-25 border-2" />
                  <span
                    className="px-2 border border-secondary rounded-1 d-flex align-items-center"
                    style={{ background: "#eee" }}
                  >
                    <BsFillCaretDownFill />
                  </span>
                  <Form.Control type="text" disabled className="w-50" />
                </Form.Group>
              </Form>
              <Form as={Col} md={10}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">XX</Form.Label>
                  {/* <Form.Control type="text" className="w-25" /> */}
                  <input type="text" className="w-25 border-2" />
                  <span
                    className="px-2 border border-secondary rounded-1 d-flex align-items-center"
                    style={{ background: "#eee" }}
                  >
                    <BsFillCaretDownFill />
                  </span>
                  <Form.Control type="text" disabled className="w-50" />
                </Form.Group>
              </Form>
              <Form as={Col} md={12}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-3">XXX</Form.Label>
                  {/* <Form.Control type="text" className="w-25 border-3  " /> */}
                  <input type="text" className="w-25 border-2" />
                  <span
                    className="px-2 border border-secondary rounded-1 d-flex align-items-center"
                    style={{ background: "#eee" }}
                  >
                    <BsFillCaretDownFill />
                  </span>
                  <Form.Control type="text" disabled className="w-50" />
                </Form.Group>
              </Form>
            </Col>
            <Col md={4}>
              <Form as={Col} md={10}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">XXXX</Form.Label>
                  {/* <Form.Control type="text" /> */}
                  <input
                    type="text"
                    className="border-2"
                    style={{ width: "125%" }}
                  />
                  <span
                    className="px-2 border border-secondary rounded-1 d-flex align-items-center"
                    style={{ background: "#eee" }}
                  >
                    <BsFillCaretDownFill />
                  </span>
                  <Form.Control type="text" disabled />
                  <span className="d-flex align-items-center px-3">XXXX</span>
                  {/* <Form.Control type="text" className="w-50" /> */}
                  <input type="text" className="w-75 border-2" />
                  <span
                    className="px-2 border border-secondary rounded-1 d-flex align-items-center"
                    style={{ background: "#eee" }}
                  >
                    <BsFillCaretDownFill />
                  </span>
                  <Form.Control type="text" disabled />
                </Form.Group>
              </Form>
              <Form as={Col} md={10}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">XXXX</Form.Label>
                  {/* <Form.Control type="text" className="w-25" /> */}
                  <input type="text" className="w-25 border-2" />
                  <span
                    className="px-2 border border-secondary rounded-1 d-flex align-items-center"
                    style={{ background: "#eee" }}
                  >
                    <BsFillCaretDownFill />
                  </span>
                  <Form.Control type="text" disabled className="w-100" />
                </Form.Group>
              </Form>
              <Form as={Col} md={10}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">
                    XX&nbsp;&nbsp;&nbsp;&nbsp;
                  </Form.Label>
                  {/* <Form.Control type="text" className="w-25" /> */}
                  <input type="text" className="w-25 border-2" />
                  <span
                    className="px-2 border border-secondary rounded-1 d-flex align-items-center"
                    style={{ background: "#eee" }}
                  >
                    <BsFillCaretDownFill />
                  </span>
                  <Form.Control type="text" disabled className="w-100" />
                </Form.Group>
              </Form>
              <Form as={Col} md={9}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">XXXX</Form.Label>
                  <Form.Control type="text" />
                  <span className="mx-2 d-flex align-items-center" r>
                    XXXXXX
                  </span>
                  <Form.Control type="text" />
                </Form.Group>
              </Form>
            </Col>
            <Col md={4}>
              <Form as={Col} md={8}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">XXXXXX</Form.Label>
                  <Form.Control type="text" />
                </Form.Group>
              </Form>
              <Form as={Col} md={8}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">
                    XXXXX&nbsp;&nbsp;
                  </Form.Label>
                  <Form.Control type="text" />
                </Form.Group>
              </Form>

              <Form as={Col} md={9}>
                <Form.Group className="my-3 d-flex">
                  <Form.Label className="mt-2 me-4">XXXXXX</Form.Label>
                  {/* <Form.Control type="text" className="w-25" /> */}
                  <input type="text" className="w-25 border-2" />
                  <span
                    className="px-2 border border-secondary rounded-1 d-flex align-items-center"
                    style={{ background: "#eee" }}
                  >
                    <BsFillCaretDownFill />
                  </span>
                  <Form.Control type="text" disabled className="w-50" />
                </Form.Group>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
      <div
        className="form-body border rounded-3 m-3 p-3"
        style={{
          background: "#fff",
        }}
      >
        <div className="header-form-body pb-3">
          <Form className="d-flex align-items-center">
            <label className="me-5 fw-bold">XX</label>
            <label className="me-2 fw-bold">XXXX</label>
            <Form.Control type="text" disabled className="w-auto " />
            <label className="px-3">XXXXXXX</label>
            <select style={{ width: "70px", padding: "6px" }}>
              <option>10X</option>
              <option>9X</option>
              <option>8X</option>
            </select>
          </Form>
        </div>
        <div
          className="table-form-body"
          style={{ height: "320px", overflowY: "scroll" }}
        >
          <table className="w-100">
            <thead>
              <tr className="bg-info">
                <th scope="col">No</th>
                <th scope="col">XXXX</th>
                <th scope="col">XXXXXXX</th>
                <th scope="col">XXXXXX</th>
                <th scope="col">XXXXXX</th>
                <th scope="col">XXXXX</th>
                <th scope="col">XXXX</th>
                <th scope="col">XXXXX</th>
                <th scope="col">XXXX</th>
                <th scope="col">XXXXX</th>
                <th scope="col">XXXX</th>
                <th scope="col">XXXXXX</th>
                <th scope="col">XXXX</th>
                <th scope="col">XXXX</th>
                <th scope="col">XXXXXX</th>
                <th scope="col">XX</th>
                <th scope="col">XXXXXX</th>
                <th scope="col">XXXXX</th>
              </tr>
            </thead>
            <tbody>
              <tr className="d-flex justify-content-start mt-2">
                <td>XXXXX</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Forms;
