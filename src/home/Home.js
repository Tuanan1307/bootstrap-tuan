import React from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
const Home = () => {
  return (
    <div className="w-100 d-flex justify-content-center  position-fixed top-50">
      <Button variant="secondary" className="m-2" size="lg">
        <Link
          className="text-white"
          to="/bootstap-basesic"
          style={{ textDecoration: "none" }}
        >
          bootstrap-basesic
        </Link>
      </Button>
      <Button variant="secondary" className="m-2" size="lg">
        <Link
          className="text-white"
          to="/bootstap-form"
          style={{ textDecoration: "none" }}
        >
          bootstrap-form
        </Link>
      </Button>
      <Button variant="secondary" className="m-2" size="lg">
        <Link
          className="text-white"
          to="/bootstap-page"
          style={{ textDecoration: "none" }}
        >
          bootstrap-page
        </Link>
      </Button>
    </div>
  );
};

export default Home;
