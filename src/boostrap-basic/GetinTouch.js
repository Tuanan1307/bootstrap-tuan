import React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const GetinTouch = () => {
  return (
    <div className="getintouch">
      <Container className="py-5">
        <Row>
          <div className="col-lg-4 col-md-4 col-sm-12 d-flex align-items-center">
            <div
              className="title text-center w-100 h1"
              style={{ color: "#F97300" }}
            >
              Get in Touch
            </div>
          </div>

          <div className="col-lg-8 col-md-8 col-sm-12">
            <Form.Group as={Col} md="9">
              <InputGroup className="mb-3" size="lg">
                <Form.Control
                  placeholder="Your Name"
                  aria-label="Username"
                  aria-describedby="basic-addon1"
                />
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="9">
              <InputGroup className="mb-3" size="lg">
                <Form.Control
                  placeholder="YourEmail@email.com"
                  aria-label="Recipient's username"
                  aria-describedby="basic-addon2"
                />
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="9">
              <InputGroup className="mb-3" size="lg">
                <Form.Control as="textarea" aria-label="With textarea" />
              </InputGroup>
            </Form.Group>

            <Button
              type="submit"
              className="btn btn-secondary btn-block w-75"
              style={{ background: "#F97300", border: "none" }}
            >
              Send
            </Button>
          </div>
        </Row>
      </Container>
    </div>
  );
};

export default GetinTouch;
