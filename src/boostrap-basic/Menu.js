import React from "react";

import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

const Menu = () => {
  return (
    <div className="menu position-sticky">
      <Navbar
        className=" position-sticky"
        collapseOnSelect
        expand="lg"
        style={{ background: "#F97300", fontWeight: 600, color: "white" }}
      >
        <Navbar.Brand href="/" className="ps-5" style={{ color: "white" }}>
          Home
        </Navbar.Brand>

        <Nav className="justify-content-end flex-grow-1 pe-5">
          <Nav.Link href="/about" style={{ color: "white" }}>
            {" "}
            About
          </Nav.Link>
          <Nav.Link href="/portfolio" style={{ color: "white" }}>
            Portfolio
          </Nav.Link>
          <Nav.Link href="/team" style={{ color: "white" }}>
            Team
          </Nav.Link>
          <Nav.Link href="/post" style={{ color: "white" }}>
            Post
          </Nav.Link>
          <Nav.Link href="/contact" style={{ color: "white" }}>
            Contact
          </Nav.Link>
        </Nav>
      </Navbar>
    </div>
  );
};

export default Menu;
