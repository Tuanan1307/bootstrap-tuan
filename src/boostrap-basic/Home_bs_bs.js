import React from "react";
import Menu from "./Menu";
import Header from "./Header";
import About from "./About";
import Portfolio from "./Portfolio";
import Blog from "./Blog";
import OurTeam from "./OurTeam";
import GetinTouch from "./GetinTouch";
import Footer from "./Footer";
const Home_bs_bs = () => {
  return (
    <>
      <Menu />
      <Header />
      <About />
      <Portfolio />
      <Blog />
      <OurTeam />
      <GetinTouch />
      <Footer />
    </>
  );
};

export default Home_bs_bs;
