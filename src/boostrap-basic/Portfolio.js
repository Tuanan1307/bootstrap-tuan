import React from "react";
import { Container, Row } from "react-bootstrap";
const Portfolio = () => {
  return (
    <div className="portfolio">
      <Container className="p-5">
        <Row>
          <div
            className="top h1 d-flex justify-content-center py-5"
            style={{ color: "#F97300" }}
          >
            Portfolio
          </div>
          <div className="bottom">
            <div className="col-12 d-flex">
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/1011-5472x3648.jpg")}
                />
              </div>
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/1004-5616x3744.jpg")}
                />
              </div>
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/1002-4312x2868.jpg")}
                />
              </div>
            </div>
            <div className="col-12 d-flex">
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/101-2621x1747.jpg")}
                />
              </div>
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/100-2500x1656.jpg")}
                />
              </div>
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/1056-3988x2720.jpg")}
                />
              </div>
            </div>
            <div className="col-12 d-flex">
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/1001-5616x3744.jpg")}
                />
              </div>
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/1056-3988x2720.jpg")}
                />
              </div>
              <div className="col-4 p-2 px-3">
                <img
                  className="w-100 m-2"
                  src={require("../accset/0-5616x3744.jpg")}
                />
              </div>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  );
};

export default Portfolio;
