import React from "react";
import { Button } from "react-bootstrap";
import "./Header.scss";
const Header = () => {
  return (
    <div className="header">
      <div className="mo"></div>
      <div className="content">
        <h1>Hello, Welcome To My Official Website</h1>
        <p className="text-black-50 w-50">
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
          cupidatat non proident, sunt in culpa qui official deserunt mollit
          anim id est laborum.
        </p>
        <div className="bottom">
          <Button
            size="lg"
            className="mt-3 see-more"
            style={{ background: "#F97300" }}
          >
            See more
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Header;
