import React from "react";

const Footer = () => {
  return (
    <div className="footer p-4" style={{ background: "#F97300" }}>
      <p className="text-center">
        Copyright&nbsp;&copy;&nbsp;2022&nbsp;ABC&nbsp;Company.
      </p>
    </div>
  );
};

export default Footer;
