import Forms from "./bootstap-form/Form";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./home/Home";
import Home_bs_bs from "./boostrap-basic/Home_bs_bs";
import Index_page from "./Boostrap-page/Index_page";
function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/bootstap-basesic" element={<Home_bs_bs />} />
          <Route path="/bootstap-form" element={<Forms />} />
          <Route path="/bootstap-page" element={<Index_page />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
