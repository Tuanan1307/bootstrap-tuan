import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FaSearch } from "react-icons/fa";
const Header_Page = () => {
  return (
    <div
      className="header-detail position-fixed top-0 w-100 py-2"
      style={{ background: "#044863" }}
    >
      <header>
        <Container fluid>
          <Row>
            <Col className="col-12 col-lg-3 col-xl-2 d-flex justify-content-center align-items-center">
              <div className="search fs-5 " style={{ color: "white" }}>
                <FaSearch />
              </div>
            </Col>

            <Col className="col-12 col-lg-9 col-xl-10">
              <Row>
                <div className="col-5 col-lg-8 col-xl-9 d-flex justify-content-center">
                  <Link to="/bootstap-page">
                    <img
                      src={require("../accset/logo.png")}
                      width="150"
                      alt="Glocal"
                    />
                  </Link>
                </div>
                <div className="col-7 col-lg-4 col-xl-3 d-flex justify-content-end align-items-center">
                  <div>
                    <Link to="#" className="me-1">
                      <img
                        src={require("../accset/appstore.png")}
                        width="130"
                        alt="ic-app-store.png"
                      />
                    </Link>
                    <Link to="#">
                      <img
                        src={require("../accset/appstore.png")}
                        width="130"
                        alt="ic-google-play.png"
                      />
                    </Link>
                  </div>
                </div>
              </Row>
            </Col>
          </Row>
        </Container>
      </header>
    </div>
  );
};

export default Header_Page;
