import React from "react";
import Detail from "./Detail";
import Header_Page from "./Header_Page";

const Index_page = () => {
  return (
    <>
      <Header_Page />
      <Detail />
    </>
  );
};

export default Index_page;
